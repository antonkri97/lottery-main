package models

// Plays model
type Plays struct {
	Fst   int    `json:"fst"`
	Snd   int    `json:"snd"`
	Thd   int    `json:"thd"`
	Email string `json:"email"`
}
