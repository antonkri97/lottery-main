package models

import (
	"gopkg.in/mgo.v2/bson"
)

// Nums model
type Nums struct {
	ID  bson.ObjectId `bson:"_id,omitempty"`
	Fst int           `json:"fst" binding:"required"`
	Snd int           `json:"snd" binding:"required"`
	Thd int           `json:"thd" binding:"required"`
}

// NumsDB модель данных из бд
type NumsDB struct {
	ID      bson.ObjectId `bson:"_id,omitempty"`
	Numbers struct {
		Fst int
		Snd int
		Thd int
	}
}

// Test test
type Test struct {
	Fst int `json:"fst" binding:"required"`
	Snd int `json:"snd" binding:"required"`
	Thd int `json:"thd" binding:"required"`
}
