FROM golang:latest

ADD . /go/src/github.com/antonkri97/lottery

ENV GOBIN /go/bin
#ENV MONGODB_URL "mongodb://mongo:27017/lottery"

RUN go get gopkg.in/gin-gonic/gin.v1 && go get gopkg.in/mgo.v2
RUN go install /go/src/github.com/antonkri97/lottery/main.go

#ENTRYPOINT /go/bin/

EXPOSE 8080

CMD ["/go/bin/main"]
