package main

import (
	"github.com/antonkri97/lottery/db"
	"github.com/antonkri97/lottery/handlers"
	"github.com/antonkri97/lottery/middlewares"
	"github.com/antonkri97/lottery/utils"
	"gopkg.in/gin-gonic/gin.v1"
)

const (
	// Port at which the server starts listening
	Port = "8080"
)

func init() {
	db.Connect()
}

func main() {
	router := gin.New()

	// Middlewares
	router.Use(middlewares.Connect)
	router.Use(middlewares.ErrorHandler)
	router.Use(middlewares.CORSMiddleware())

	// Routes
	router.GET("/hello", handlers.Hello)
	router.POST("/numbers", handlers.Insert)
	router.GET("/numbers", handlers.Numbers)
	router.POST("/test", handlers.Test)

	port := utils.CheckEnv("PORT", Port)

	router.Run(":" + port)
}
