package handlers

import (
	"net/http"

	"log"

	"github.com/antonkri97/lottery/models"
	"github.com/antonkri97/lottery/utils"

	"gopkg.in/gin-gonic/gin.v1"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// Insert player's nums to db
func Insert(c *gin.Context) {
	db := c.MustGet("db").(*mgo.Database)
	play := models.Plays{}
	numbs := models.NumsDB{}

	if c.BindJSON(&play) == nil {
		err := db.C("plays").Find(nil).Select(bson.M{"numbers": 1}).Sort("-$natural").Limit(-1).One(&numbs)
		if err != nil {
			log.Fatal(err)
			return
		}

		grp, err := utils.PlayerResult(play, numbs)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		in, err := utils.AlreadyInserted(db, numbs.ID, grp, play)
		if err != nil {
			c.Status(http.StatusInternalServerError)
			return
		}
		if in {
			c.JSON(http.StatusOK, gin.H{"status": "already inserted"})
			return
		}

		err = db.C("plays").UpdateId(
			numbs.ID,
			bson.D{{"$push", bson.D{{grp, bson.D{{"fst", play.Fst}, {"snd", play.Snd}, {"thd", play.Thd}, {"email", play.Email}}}}}},
		)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "can't insert player's result to db"})
			return
		}

		c.Status(http.StatusOK)
	}
}

//Numbers is handler to get main application numbers
func Numbers(c *gin.Context) {
	db := c.MustGet("db").(*mgo.Database)
	nums := models.Nums{}

	err := db.C("nums").Find(nil).One(&nums)
	if err != nil {
		c.JSON(http.StatusConflict, gin.H{
			"error": err.Error(),
		})
	} else {
		c.JSON(http.StatusOK, nums)
	}
}

// Test is for test
func Test(c *gin.Context) {
	db := c.MustGet("db").(*mgo.Database)
	numbs := models.NumsDB{}

	err := db.C("plays").Find(nil).Select(bson.M{"numbers": 1}).Sort("-$natural").Limit(-1).One(&numbs)
	if err != nil {
		log.Fatal(err)
		return
	}

	err = db.C("plays").UpdateId(
		numbs.ID,
		bson.D{{"$push", bson.D{{"none", bson.D{{"fst", 1}, {"snd", 1}, {"thd", 1}, {"email", "anton"}}}}}},
	)

	c.Status(http.StatusOK)
}

// Hello to user
func Hello(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"message": "Hi!",
	})
}
