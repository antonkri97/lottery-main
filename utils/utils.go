package utils

import (
	"errors"
	"log"

	"os"

	"github.com/antonkri97/lottery/models"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// PlayerResult определяет в какую группу в бд стоит записать игрока
// Возвращает:
// "threeNums" в случае джекпота;
// "twoNums", если игрок отгадал два числа;
// "none" в случае неудачи
func PlayerResult(player models.Plays, nmb models.NumsDB) (string, error) {
	result := 0

	if player.Fst == nmb.Numbers.Fst {
		result++
	}
	if player.Snd == nmb.Numbers.Snd {
		result++
	}
	if player.Thd == nmb.Numbers.Thd {
		result++
	}

	switch result {
	case 0, 1:
		return "none", nil
	case 2:
		return "twoNums", nil
	case 3:
		return "threeNums", nil
	default:
		return "", errors.New("can't get player's result")
	}
}

// CheckEnv возвращает значение по-умолчанию, если переменная окружения не опеределена
// либо саму переменную окружения
func CheckEnv(env string, def string) string {
	e := os.Getenv(env)
	log.Println(e)
	if len(e) == 0 {
		return def
	}
	return e
}

// AlreadyInserted возвращает true, если пользователь сегодня уже занес такие цифры в бд
func AlreadyInserted(db *mgo.Database, id bson.ObjectId, playerGroup string, play models.Plays) (bool, error) {
	n, err := db.C("plays").Find(
		bson.D{{"$and", []bson.D{{{"_id", id}, {playerGroup, bson.D{{"$in", []bson.D{{{"fst", play.Fst}, {"snd", play.Snd}, {"thd", play.Thd}, {"email", play.Email}}}}}}}}}},
	).Count()

	if err != nil {
		log.Fatal(err)
		return false, err
	}

	return n == 1, nil
}
